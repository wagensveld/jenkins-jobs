# Jenkins Jobs

This is just an example repository to use as a base for seeding Jenkins jobs.

# Setup

First thing which you need to do is install the `Job DSL` plugin:
Go to `Manage Jenkins` -> `Manage Plugins` -> `Available`, and install the `Job DSL` plugin without restart.

Afterwards create the DSL job with `New Item`:

```
Enter an item name: dsl-seed-jenkins-jobs
Freestyle project
```

Under `Build` click `Add build step` -> `Process Job DSLs`, select `Use the provided DSL script`

DSL Script:

```groovy
pipelineJob('example-job-1') {
  definition {
    cpsScm {
      scm {
        git('https://gitlab.com/wagensveld/jenkins-jobs.git', 'master') { node ->
          node / gitConfigName('DSL User')
          node / gitConfigEmail('me@me.com')
        }
      }
      scriptPath('jobs/example-job-1/Jenkinsfile')
    }
  }
}

pipelineJob('example-job-2') {
  definition {
    cpsScm {
      scm {
        git('https://gitlab.com/wagensveld/jenkins-jobs.git', 'master') { node ->
          node / gitConfigName('DSL User')
          node / gitConfigEmail('me@me.com')
        }
      }
      scriptPath('jobs/example-job-2/Jenkinsfile')
    }
  }
}
```

You may want to configure `Action for removed jobs/views/config files`

Click `Save` and then `Build` and now you'll have fresh Jenkins jobs.

## Other tips

Go to `http://yourjenkinsurl.com/plugin/job-dsl/api-viewer/index.html` to see the API reference, if using Github rather than Gitlab, you'll see theres no need to use the `gitConfigName` and `gitConfigEmail`.

If you want a subdirectory, first create the subdirectory in the Jenkins console, then do something like this `pipelineJob('subdirectory/example-job-2')`

The process for this changes depending on your git solution, but set up a build trigger for the DSL job on git push.
